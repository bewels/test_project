import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { envSchema } from './core/validations/env.validation';
import { SequelizeModule } from '@nestjs/sequelize';
import { dbConnect } from './core/configs/db.config';
import { CategoriesModule } from './subdomains/categories/categories.module';


@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: envSchema
    }),
    SequelizeModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: dbConnect
    }),
    CategoriesModule
  ],
})
export class AppModule {}
