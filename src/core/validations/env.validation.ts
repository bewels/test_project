import * as joi from 'joi'

export const envSchema = joi.object({
	DB_HOST: joi.string(),
	DB_PORT: joi.number().port(),
	DB_USER: joi.string(),
	DB_PASS: joi.string(),
	DB_NAME: joi.string()
})