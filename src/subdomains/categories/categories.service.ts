import { HttpCode, HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Op } from 'sequelize';
import { Sequelize } from 'sequelize-typescript';
import { ChangeCategoryDto } from './dto/changeCategory.ato';
import { CreateCategoryDto } from './dto/createCategory.dto';
import { GetCategoriesByParamsDto } from './dto/getCategoriesByParams.dto';
import { CategoryModel } from './model/category.model';

@Injectable()
export class CategoriesService {
	
	constructor(
		@InjectModel(CategoryModel) private readonly categories: typeof CategoryModel
	) {}

	async createCategory(body: CreateCategoryDto) {
		const category = await this.categories.findOne({where: {slug: body.slug}})

		if(category) throw new HttpException('Category whit this slug already exist', HttpStatus.BAD_REQUEST)

		return await this.categories.create(body)
	}

	async changeCategory(body: ChangeCategoryDto, id: string) {
		const category = await this.categories.findByPk(id)

		if(!category) throw new HttpException('Category not found', HttpStatus.NOT_FOUND)

		category.setAttributes({
			...body
		})

		return await category.save()
	}

	async deleteCategory(id: string) {
		const category = await this.categories.findByPk(id)

		if(!category) throw new HttpException('Category not found', HttpStatus.NOT_FOUND)
		await category.destroy()

		return {deleted: category.id}
	}

	async getCategory(id: string) {
		const category = await this.categories.findByPk(id)

		if(!category) throw new HttpException('Category not found', HttpStatus.NOT_FOUND)

		return category
	}

	async getCategories(query: GetCategoriesByParamsDto) {

		const searchForm = query.search ? 
		[
			this.generateSearchCondition('name', query.search),
			this.generateSearchCondition('description', query.search),
		]
		:
		[
			this.generateSearchCondition('name', query.name),
			this.generateSearchCondition('description', query.description),
		]

		const sortDirection = query.sort?.charAt(0) === '-' ? 'DESC' : 'ASC' 
		const sortField = query.sort?.replace('-', '')

		const activeFilter = <{active: boolean}> {}

		if(query.active === 'true' || query.active === '1') activeFilter.active = true
		else if (query.active === 'false' || query.active === '0') activeFilter.active = false

		const page = !+query.page ? 0 : +query.page - 1
		return await this.categories.findAll({
			limit: +query.pageSize || 2,
			offset: page,
			where: {
				[Op.and]: {
					...searchForm.filter(item => item),
					...activeFilter
				}
			},
			order: sortField ? [[sortField, sortDirection]] : null
		})
	}

	private generateSearchCondition(field: string, condition: string) {
		if(!condition) return 
		return Sequelize.where(
			Sequelize.fn('replace', Sequelize.fn('lower', Sequelize.col(field)), 'ё', 'е'),
			{
			    [Op.like]: `%${condition.toLowerCase()}%`
		    }
		)
	}

}
