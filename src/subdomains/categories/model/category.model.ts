import { Column, CreatedAt, DataType, Model, Table } from "sequelize-typescript";

@Table({
	tableName: 'category',
	updatedAt: false
})
export class CategoryModel extends Model<CategoryModel> {
	@Column({
		type: DataType.UUID,
		defaultValue: DataType.UUIDV4(),
		allowNull: false,
		unique: true,
		primaryKey: true
	})
	id: string
	
	
	@Column({
		type: DataType.STRING(256),
		allowNull: false,
		unique: true,
	})
	slug: string

	@Column({
		type: DataType.STRING(256),
		allowNull: false,
	})
	name: string

	@Column({
		type: DataType.STRING(1024),
		allowNull: true,
	})
	description?:string
	
	@CreatedAt
	createdDate: Date
	
	
	@Column({
		type: DataType.BOOLEAN,
		allowNull: false,
	})
	active!: boolean


}