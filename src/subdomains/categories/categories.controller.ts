import { Body, Controller, Delete, Get, Param, ParseUUIDPipe, Patch, Post, Query } from '@nestjs/common';
import { CategoriesService } from './categories.service';
import { ChangeCategoryDto } from './dto/changeCategory.ato';
import { CreateCategoryDto } from './dto/createCategory.dto';
import { GetCategoriesByParamsDto } from './dto/getCategoriesByParams.dto';

@Controller('categories')
export class CategoriesController {
	constructor(
		private readonly categoriesService: CategoriesService
	) {}

	@Post('/create')
	async crateCategory(@Body() body: CreateCategoryDto) {
		return await this.categoriesService.createCategory(body)
	}

	@Patch('/change/:id')
	async changeCategory(@Body() body: ChangeCategoryDto, @Param('id', new ParseUUIDPipe()) id: string) {
		return await this.categoriesService.changeCategory(body, id)
	}

	@Delete('/delete/:id')
	async deleteCategory(@Param('id', new ParseUUIDPipe()) id: string) {
		return await this.categoriesService.deleteCategory(id)
	}

	@Get('/getOne/:id')
	async getCategoryById(@Param('id', new ParseUUIDPipe()) id: string) {
		return await this.categoriesService.getCategory(id)
	}

	@Get('/getByQuery')
	async getAllCategories(@Query() query: GetCategoriesByParamsDto) {
		return await this.categoriesService.getCategories(query)
	}
}
