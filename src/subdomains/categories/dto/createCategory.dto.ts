import { IsBoolean, IsEmpty, IsNotEmpty, IsOptional, IsString } from "class-validator"

export class CreateCategoryDto {

	@IsString({
		message: 'Slug should be a string'
	})
	@IsNotEmpty({
		message: 'Slug should be a not empty'
	})
	slug: string
	
	
	@IsString({
		message: 'name should be a string'
	})
	@IsNotEmpty({
		message: 'name should be a not empty'
	})
	name: string
	
	
	@IsString({
		message: 'description should be a string'
	})
	@IsOptional()
	description?:string
	
	@IsBoolean({
		message: 'active should be a boolean'
	})
	@IsNotEmpty({
		message: 'active should be a not empty'
	})
	active: boolean

}