import { IsNumber, IsOptional, IsString, Max } from "class-validator"

export class GetCategoriesByParamsDto {
	@IsString()
	@IsOptional()
	name?: string

	@IsString()
	@IsOptional()
	description?: string

	@IsString()
	@IsOptional()
	active?: string

	@IsString()
	@IsOptional()
	search?: string
	
	@IsString()
	@IsOptional()
	pageSize?: string
	
	@IsString()
	@IsOptional()
	page?: string

	@IsString()
	@IsOptional()
	sort?: string
}