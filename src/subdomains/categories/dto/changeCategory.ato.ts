import { IsBoolean, IsOptional, IsString } from "class-validator"

export class ChangeCategoryDto {

	@IsString({
		message: 'Slug should be a string'
	})
	@IsOptional()
	slug?: string
	
	
	@IsString({
		message: 'name should be a string'
	})
	@IsOptional()
	name?: string
	
	
	@IsString({
		message: 'description should be a string'
	})
	@IsOptional()
	description?:string
	
	@IsBoolean({
		message: 'active should be a boolean'
	})
	@IsOptional()
	active?: boolean

}